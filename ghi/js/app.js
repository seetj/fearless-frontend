function createCard(name, description, pictureUrl,startDate,endDate,location) {
    return `
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
      </div>
      <div class="card-footer">
        ${startDate} - ${endDate}
      </div>
    `;
  }

  function createAlert(e) {
    return `
      <div class="alert alert-warning" role="alert">
        Error: ${e}
      </div>
    `;
}



  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const endDate = new Date(details.conference.ends);
            const startDateStr = startDate.toLocaleDateString();
            const endDateStr = endDate.toLocaleDateString();
            const location = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDateStr, endDateStr,location);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }
  
      }
    } catch (e) {
      console.error('error', error);
      const htmlAlert = createAlert('There is error');
      const divAlert = document.querySelector('.error-alert');
      divAlert.innerHTML = htmlAlert;
    }
  });