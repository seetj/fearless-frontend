import React, { useEffect, useState } from 'react';
function PresentationForm() {

    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {}
        data.presenter_name = presenterName;
        data.company_name = companyName;
        data.presenter_email = presenterEmail;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;
        console.log(data);
        
        
      const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations`;
      const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
          },
      };
      const response = await fetch(presentationUrl, fetchConfig);
      if (response.ok) {
          const newPresentation = await response.json();
          console.log(newPresentation)

          setPresenterName('');
          setCompanyName('');
          setPresenterEmail('');
          setTitle('');
          setSynopsis('');
          setConference('');
    }
    
    }

    const [presenterName,setPresenterName] = useState('');
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }

    const [companyName,setCompanyName] = useState('');
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const [presenterEmail,setPresenterEmail] = useState('');
    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }

    const [title,setTitle] = useState('');
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const [synopsis,setSynopsis] = useState('');
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const [conference,setConference] = useState('');
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const[conferences, setConferences] = useState([])
    const fetchData = async () => {
      const url = "http://localhost:8000/api/conferences/"
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences)
      }
    }

  useEffect(() => {fetchData();}, []);

  return(
    <>
    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Create a new presentation</h1>
        <form onSubmit={handleSubmit} id="create-location-form">
        <div className="form-floating mb-3">
            <input onChange={handlePresenterNameChange} placeholder="Presenter Name"
            required type="text" name="presenter_name" id="presenter_name"
            className="form-control" value={presenterName} />
            <label htmlFor="presenter_name">Presenter Name</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handlePresenterEmailChange} placeholder="Presenter Email"
            required type="text" name="presenter_email" id="presenter_email"
            className="form-control" value={presenterEmail} />
            <label htmlFor="presenter_email">Presenter Email</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleCompanyNameChange} placeholder="Company Name"
            required type="text" name="company_name" id="company_name"
            className="form-control" value={companyName} />
            <label htmlFor="company_name">Company Name</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleTitleChange} placeholder="Title"
            required type="text" name="title" id="title"
            className="form-control" value={title} />
            <label htmlFor="title">Title</label>
        </div>
        <div className="form-floating mb-3">
            Synopsis
            <textarea onChange={handleSynopsisChange}
            required type="text" name="synopsis" id="synopsis"
            className="form-control" value={synopsis}></textarea>
            <label htmlFor="synopsis"></label>
        </div>
        <div className="mb-3">
            <select value={conference} onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
            <option>Choose a conference</option>
            {conferences.map(conference => {
                return (
                <option key={conference.id} value={conference.id} >
                {conference.name}
                </option>
                );
            })}
            </select>
        </div>
        <button className="btn btn-primary">Create</button>
        </form>
    </div>
    </div>
    </div>
    </>
  )
}

export default PresentationForm